import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import {
  AddInfoModalComponent,
  SuccessModalComponent,
} from "@bni-maverick/lib";
import { FileUpload } from "dist/lib/assets/model/file-upload.model";
import { interval } from "rxjs";
import { takeWhile } from "rxjs/operators";

@Component({
  selector: "app-modal-button",
  templateUrl: "./modal-button.component.html",
  styleUrls: ["./modal-button.component.scss"],
})
export class ModalButtonComponent implements OnInit {
  currentFileUpload?: FileUpload;

  isUploaded: boolean;
  progress = 0;
  timeLeft = 10;
  interval$;

  constructor(
    private dialog: MatDialog  ) {}

  ngOnInit(): void {
    console.log("hello");
  }

  openPopup() {
    this.isUploaded = false;
    const ref = this.dialog.open(AddInfoModalComponent, {
      width: "60%",
      enterAnimationDuration: 500,
      exitAnimationDuration: 500,
    });

    const succ = ref.componentInstance.submitReason.subscribe((event) => {
        ref.close()
        ref.componentInstance.isLoading = false;
        this.openSuccess()
        console.log(event);
    });

    const subs = ref.componentInstance.selectedFile.subscribe((event) => {
      if (event) {
        this.currentFileUpload = new FileUpload(event);
        this.interval$ = interval(1000)
          .pipe(takeWhile(() => this.progress < 100))
          .subscribe(() => {
            this.progress += 10;
            ref.componentInstance.percentageSub.next(this.progress);
            ref.componentInstance.currentFileUpload = this.currentFileUpload;
            this.timeLeft -= 1;
          });
      }
    });

    ref.afterClosed().subscribe(() => {
      subs.unsubscribe();
      succ.unsubscribe();
    });
  }

  openSuccess() {
    const succ = this.dialog.open(SuccessModalComponent, {
      width: "40%",
      data: {
        state: "Berhasil",
        message: "contoh message",
      },
    });
    succ.afterClosed().subscribe()
  }
}
