import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ButtonComponent } from './pages/button/button.component';
import { ModalButtonComponent } from './pages/modal-button/modal-button.component';

const routes: Routes = [
  {
    path:'button', component: ButtonComponent
  },
  {
    path:'modal-button', component:ModalButtonComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibComponentsRoutingModule { }
