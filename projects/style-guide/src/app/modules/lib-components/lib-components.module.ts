import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MatCardModule } from "@angular/material/card";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatDialogModule } from "@angular/material/dialog";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { LibComponentsRoutingModule } from './lib-components-routing.module';
import { ButtonComponent } from './pages/button/button.component';
import { ButtonModule, AddInfoModalModule } from '@bni-maverick/lib';
import { ModalButtonComponent } from './pages/modal-button/modal-button.component';

const materialModules = [
  MatCardModule,
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatListModule,
  MatIconModule,
  MatDialogModule,
];

@NgModule({
  declarations: [
    ButtonComponent,
    ModalButtonComponent
  ],
  imports: [
    CommonModule,
    LibComponentsRoutingModule,
    ButtonModule,
    AddInfoModalModule,
    ...materialModules,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LibComponentsModule { }
