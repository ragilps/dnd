import { Component, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { FileUpload } from '../../assets/model/file-upload.model';

@Component({
  selector: 'lib-add-info-modal',
  templateUrl: './add-info-modal.component.html',
  styleUrls: ['./add-info-modal.component.scss']
})
export class AddInfoModalComponent {
  @Input() currentFileUpload?: FileUpload;
  @Input() percentageSub = new Subject<number>();
  @Input() isLoading: boolean;
  @Input() isError: boolean;
  @Input() isFileTypeValid: boolean;
  @Input() isFileSizeValid: boolean;


  infoForm: FormGroup;
  remainingText : number

  constructor(private formBuilder : FormBuilder) {
    //
  }

  selectedFile = new EventEmitter();
  dragFile = new EventEmitter();
  submitReason = new EventEmitter();

  ngOnInit(): void {
    // this.isUploaded = false
    this.infoForm = this.formBuilder.group({
      info : [''],
      file : ['']
    })
  }

  submit(value) {
    if (this.isFileSizeValid && this.isFileTypeValid) {
      this.isLoading = true;
      setTimeout(() => {
        this.submitReason.emit(value);
      }, 3000);
    } else {
      this.addInfo();
    }
  }

  addInfo() {
    this.isLoading = true;
    this.isError = false;
    setTimeout(() => {
      this.isLoading = false;
      this.isError = true;
    }, 3000);
  }

  onDragSuccess(event) {
    const files: FileList = event.dataTransfer.files;
    const file: File = files[0];
    this.isFileTypeValid = this.fileType(file);
    this.isFileSizeValid = this.fileSize(file);
    if (this.isFileTypeValid && this.isFileSizeValid) {
      this.dragFile.emit(file);
    }
  }

  submitUserFile(event): void {
    const file: File = event.target.files.item(0);
    this.isFileTypeValid = this.fileType(file);
    this.isFileSizeValid = this.fileSize(file);
    if (this.isFileTypeValid && this.isFileSizeValid) {
      this.selectedFile.emit(file);
    }
  }

  myChange(val) {
    this.remainingText = 200 - val.length;
  }

  private fileSize(file: File) {
    const maxSize = 5 * 1024 * 1024; // 5MB
    return file.size <= maxSize;
  }

  private fileType(file: File): boolean {
    const allowedTypes = [
      'application/pdf',
      'image/jpeg',
      'image/jpg',
      'image/png',
      'image/tiff',
    ];

    return allowedTypes.includes(file.type);
  }
}
