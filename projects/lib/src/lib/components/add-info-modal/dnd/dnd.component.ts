import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { FileUpload } from '../../../assets/model/file-upload.model';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'bni-dnd',
  templateUrl: './dnd.component.html',
  styleUrls: ['./dnd.component.scss']
})
export class DndComponent {

  @Input() currentFileUpload?: FileUpload;
  @Input() percentageSub = new Subject<number>();
  @Input() isLoading: boolean;
  @Input() isError: boolean;
  @Input() isFileTypeValid: boolean;
  @Input() isFileSizeValid: boolean;

  isHighlight: boolean;
  isUploading: boolean;

  @Output() selectedFile = new EventEmitter();
  @Output() dragFile = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
    this.isUploading=false;
    console.log("");
  }

  onDragOver(event) {
    this.isHighlight = true;
    event.preventDefault();
  }
  onDragLeave(event) {
    this.isHighlight = false;
    event.preventDefault();
  }

  onDragSuccess(event) {
    event.preventDefault();
    this.isUploading=true;
    this.dragFile.emit(event);
  }

  upload(event) {
    this.isUploading=true;
    this.selectedFile.emit(event);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
}
