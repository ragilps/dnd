import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInfoModalComponent } from './add-info-modal.component';

describe('AddInfoModalComponent', () => {
  let component: AddInfoModalComponent;
  let fixture: ComponentFixture<AddInfoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInfoModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
