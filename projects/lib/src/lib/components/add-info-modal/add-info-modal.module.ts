import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddInfoModalComponent } from './add-info-modal.component';

import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from "@angular/material/icon";
import { MatDialogModule } from "@angular/material/dialog";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DndComponent } from './dnd/dnd.component';


const materialModules = [
  MatCardModule,
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatListModule,
  MatIconModule,
  MatDialogModule,
  MatProgressSpinnerModule
];


@NgModule({
  declarations: [
    AddInfoModalComponent,
    DndComponent
  ],
  imports: [
    CommonModule,
    ...materialModules,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    AddInfoModalComponent,
    DndComponent
  ]
})
export class AddInfoModalModule { }
