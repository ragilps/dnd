import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'lib-success-modal',
  templateUrl: './success-modal.component.html',
  styleUrls: ['./success-modal.component.scss']
})
export class SuccessModalComponent implements OnInit{

  constructor(@Inject (MAT_DIALOG_DATA) private data){

  }

  modalData;

  ngOnInit(): void {
      this.modalData = this.data
  }

}
