# Maverick Back Office Library

This application contains library to share reusable components, styles, pipes, services across Micro Frontends. There are two projects:

- The library which contains the shared library and will be publish to BNI gitlab package registry
- Style Guide which will give description on how to use the library

## Tools for Development

- Node
- Angular CLI
- Visual Studio Code
- Visual Studio Extensions
  - ESLint
  - Sonarlint
  - Prettier (_optional_)
  - GitLens (_optional_)

## Code Guideline

- Avoid using type `any`
- Use **Reactive Form** instead of **Template Driven Form**
- Put id for all possible component for automation purpose
- Indent the code using double spaces
- DRY (do not repeate yourself) as much as possible
- Write unit test for all possible **components**, **pipes**, **directives**, **utils** with minimum total coverage of 70%
- Use **camelCase** for variables
- Use **SCSS** file for styling
- Fix all **SonarLint**, **ESLint** and **Unit Test** issues before raising merge request
    run commands to run eslint and unit test as defined on the **Commands** section
- Use **Angular Material** for components and **Bootstrap** for grid and responsive design
- After creating a feature in library makes sure to make the demo and define of how to use it in the style-guide project

## Folder Structure

- Put all text displayed on the HTML on `src/lib/assets/id.json` and `src/lib/assets/en.json` and use ngx-translate pipe
- Put arbitrary number or parameter on `src/lib/constants` folder, eq: Regex, Min and Max validation
- For assets like fonts, or icons put inside `/src/lib/assets/fonts`, `/src/lib/assets/icons` respectively
- For components, services, directives, models, and pipes put inside `/src/lib/components|services|directives|models|pipes` with it's own modules (except models)
- For more detail on the folder structure refer to this link <https://medium.com/@shijin_nath/angular-right-file-structure-and-best-practices-that-help-to-scale-2020-52ce8d967df5>

## Steps to Run The Style Guide Project
- `ng build lib --configuration development --watch
- `ng serve --project style-guide`
- import the required module from `@bni-maverick/lib` in the target style guide module to demo the component / pipe / directive, etc

## Steps to Publish the Library

- Increment the version of the lib in the `projects/lib/package.json` by:
  - `cd projects/lib/`
  - `npm version <new_version>`
- Go back to the root folder `cd ../../`
- `ng build lib --configuration production`
- `cd dist/lib`
- Add `.npmrc` on the directory with details to the package registry with the correct auth token
- `npm publish`

## Commands

| Command | Description |
| ------ | ------ |
| `npm install` | To install the dependency packages |
| `ng generate component | directive | pipe | service | class | guard | interface | enum | module --project lib` | To generate component/ directive/ pipe / service, etc and define which project |
| `ng lint` | To run ESLint checking |
| `ng test` | To execute the unit tests via [Karma](https://karma-runner.github.io) |
| `ng serve --project style-guide` | To run the style guide locally |
| `ng build style-guide --configuration production` | To build the style guide application |
| `ng build lib --configuration production` | To build the lib project |
| `ng build lib --configuration development --watch` | To build the lib project and live rebuild on code changes for development purpose |
| `npm publish` | To publish new version of the lib as npm package |
| `npm version <new_version>` | To update the version in package.json
